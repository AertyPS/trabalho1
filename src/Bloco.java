public class Bloco {
    private Bloco proximo;
    private int numero;

    public Bloco(Bloco prox,int num){
        this.numero = num;
        this.proximo = prox;
    }

    
    public Bloco getProximo() {
        return proximo;
    }

    
    public void setProximo(Bloco proximo) {
        this.proximo = proximo;
    }

    
    public int getNumero() {
        return numero;
    }

    
    public void setNumero(int numero) {
        this.numero = numero;
    }

}
