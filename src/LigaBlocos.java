public class LigaBlocos {

    private Bloco primeiro;
    private Bloco ultimo;
    private int totalBlocos;

    public LigaBlocos() {}

    public void addComeco(int numero) {// adiciona o novo bloco no começo
        Bloco novo = new Bloco(this.primeiro, numero);
        this.primeiro = novo;

        if (totalBlocos == 0) {// lista esteja vazia
            this.ultimo = this.primeiro;
        }
        this.totalBlocos++;

    }

    public void adicionar(int numero) {// adiciona o numero do bloco no fim da lista
        if (totalBlocos == 0) {
            addComeco(numero);
        } else {
            Bloco novo = new Bloco(this.primeiro, numero);
            this.ultimo.setProximo(novo);// coloco no fim da lista
            this.ultimo = novo;// coloco no fim da lista nessa classe
            this.totalBlocos++;
        }
    }

    public String mostrar() {// mostra o que contem nos blocos

        if (totalBlocos == 0) {// verifica se ha blocos
            return "";
        }

        StringBuilder texto = new StringBuilder();// para fazer uma concatenaçao
        Bloco atual = primeiro;

        for (int i = 0; i < this.totalBlocos - 1; i++) {// percorre ate o penultimo
            texto.append(atual.getNumero());// adiciono ao texto o primeiro da lista
            texto.append(" ");// espaço no texto separa os numeros
            atual = atual.getProximo();// pulo del gato
        }

        texto.append(atual.getNumero());// ultimo bloco

        return texto.toString();
    }

    private boolean posicaoOcupada(int pos) {// verifica se tem bloco na posicao
        if ((pos >= 0) && (pos < this.totalBlocos)) {
            return true;
        }
        return false;

    }

    public int pegar(int pos) {// pega o conteudo do bloco
        return this.pegaBloco(pos).getNumero();

    }

    public int tamanho() {
        return this.totalBlocos;
    }

    private Bloco pegaBloco(int pos) {// pegar o bloco na posicao
        if (!this.posicaoOcupada(pos)) {
            throw new IllegalArgumentException("Posiçao nao existe");
        }

        Bloco atual = primeiro;
        for (int i = 0; i < pos; i++) {
            atual = atual.getProximo();
        }
        return atual;
    }

    public void addPosDesejada(int pos, int num) {// adicionar na posicao desejada
        if (pos == 0) {
            this.addComeco(num);// adiciona no começo
        } else if (pos == this.totalBlocos) {// adiciona no fim
            this.adicionar(num);
        } else {
            Bloco anterior = this.pegaBloco(pos - 1);// pego o bloco na posicao anterior
            Bloco novo = new Bloco(anterior.getProximo(), num);// pego o novo bloco aponto para onde o anterior apontava
            anterior.setProximo(novo);// aponto o bloco anterior para o novo bloco
            this.totalBlocos++;// adiciono o novo bloco na contagem
        }
    }

    public void removeComeco() {// remove um bloco do começo

        if (!this.posicaoOcupada(0)) {
            throw new IllegalArgumentException("Essa posicao nao existe");
        }

        this.primeiro = this.primeiro.getProximo();
        this.totalBlocos--;

        if (this.totalBlocos == 0) {
            this.ultimo = null;
        }
    }

    public boolean contem(int numero) {// retorna verdadeiro ou false se tiver um bloco
        if (totalBlocos == 0) {// verifica se ha blocos
            return false;
        }

        Bloco atual = primeiro;

        for (int i = 0; i < this.totalBlocos ; i++) {// percorre ate o penultimo
             if(atual.getNumero() == numero){
                return true;
             }// adiciono ao texto o primeiro da lista
            
            atual = atual.getProximo();// pulo del gato
        }

        //atual.getNumero();// ultimo bloco
        return false;
    }

    public int Indice(int numero) {// retorna o indice se tiver 

        if (totalBlocos == 0) {// verifica se ha blocos
            return -1;
        }

        Bloco atual = primeiro;

        for (int i = 0; i < this.totalBlocos ; i++) {
             if(atual.getNumero() == numero){
                return i;
             }
            
            atual = atual.getProximo();// pulo del gato
        }

        return -1;
    }

}
