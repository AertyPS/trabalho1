import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
//a maioria dos imports sao para manipulaçao dos arquivos tanto leitura quanto escrita
public class Controle {
    
    private LigaBlocos bloco[];// inicializaçao de um vetor da classe ligaBlocos
    private int qtd;//quantidade do arquivo

    public Controle() {}

    public void Leitura() throws IOException {

        FileReader fr = null;
        BufferedReader br = null;

        try {

            fr = new FileReader("entrada.txt");
            br = new BufferedReader(fr);

            String linha = br.readLine();
            this.qtd = Integer.parseInt(linha);
            CriaBlocos(this.qtd);// criação dos blocos

            linha = br.readLine();

            int contador = 0, pNumero = 0, sNumero = 0;
            String pComando = "", sComando = "", letra = "", fim = "";

            while (linha != null) {// dessossador de arquivo

                StringReader reader = new StringReader(linha);
                int k = 0;

                while ((k = reader.read()) != -1) { // desossando a linha

                    if ((char) k != ' ') {

                        letra += (char) k;

                    } else {

                        if (contador == 0) {// pego a primeira palavra
                            pComando = letra;
                           // System.out.println(letra);
                        } else if (contador == 1) {// pego a segunda palavra
                            pNumero = Integer.parseInt(letra);
                           // System.out.println(letra);
                        } else if (contador == 2) {// pego a terceira palavra
                            sComando = letra;
                           // System.out.println(letra);
                        }

                        letra = "";
                        contador++;
                    }

                }

                if (contador > 2) {// pego a quarta palavra

                    sNumero = Integer.parseInt(letra);
                   // System.out.println(letra);
                    letra = "";

                } else {// pego o quit -->aqui pois o quit nao tem espaço o mesmo vale para o ultimo
                        // numero

                    fim = letra;//para pegar o quit e finalizar
                  //  System.out.println(letra + "\n");
                    letra = "";
                }

                contador = 0;
                linha = br.readLine();

                if(fim.equals("quit")){
                    escrita();
                }else if(!ignore( pNumero, sNumero)){
                    comando(pComando, sComando, pNumero, sNumero);
                }
                
            }

        } catch (FileNotFoundException e) {
            System.out.println("Arquivo nao encontrado");

        } catch (IOException e) {
            System.out.println("Erro na leitura");

        } finally {

            fr.close();
            br.close();
        }
    }

    public void CriaBlocos(int qtd) {// criaçao dos blocos
        this.bloco = new LigaBlocos[qtd];

        for (int i = 0; i < qtd; i++) {
            this.bloco[i] = new LigaBlocos();// gero os blocos dentro dos blocos
            this.bloco[i].addComeco(i);
        }

        for (int i = 0; i < qtd; i++) {

            System.out.print(i + ": " + this.bloco[i].mostrar());
            System.out.print("\n");
        }
    }

    public void comando(String p, String m, int n1, int n2) {

        if (p.equals("move")) {
            if (m.equals("onto")) {
                int novoIndice = 0;
                retorno2(this.bloco[n1].tamanho(),n1);
                novoIndice = retorno2(this.bloco[n2].tamanho(),n2);
                this.bloco[novoIndice].adicionar(n2);
                this.bloco[novoIndice].adicionar(n1);
                System.out.print("\n");

            } else if (m.equals("over")) {
                retorno2(this.bloco[n1].tamanho(),n1);
                int novoIndice = IndiceAgora(n2);
                this.bloco[novoIndice].adicionar(n1);
                System.out.print("\n");
            }

        }else if(p.equals("pile")){

            if (m.equals("onto")) {
                
                int novoIndice  = retorno2(this.bloco[n2].tamanho(),n2);
                LigaBlocos bRet = retornoPilha2(n1);//vetor de blocos acima
                int tam = bRet.tamanho();
                
                this.bloco[novoIndice].adicionar(n2);
                this.bloco[novoIndice].adicionar(n1);

                if(tam > 0){
                    
                    for (int i = 0 ; i < tam; i++) {
                    this.bloco[novoIndice].adicionar(bRet.pegar(i));
                    }
                } 
            
                System.out.print("\n");

            } else if (m.equals("over")) {
                
                int novoIndice  = IndiceAgora(n2);
                LigaBlocos bRet = retornoPilha2(n1);//vetor de blocos acima
                int tam = bRet.tamanho();
                this.bloco[novoIndice].adicionar(n1);

                if(tam > 0){
                    
                    for (int i = 0 ; i < tam; i++) {
                    this.bloco[novoIndice].adicionar(bRet.pegar(i));
                    }
                } 
            
                System.out.print("\n");
            }
        }


        for (int i = 0; i < qtd; i++) {//mostro os novos arranjos

            System.out.print(i + ": " + this.bloco[i].mostrar());
            System.out.print("\n");
        }
    }

    public void retorno(int tam, int n){

        if (tam > 0) {//vejo o tamanho do vetor
         
            int blocoX = 0;
            for (int i = (tam-1); i >= 0; i--) {
                blocoX = this.bloco[n].pegar(i);

                if(this.bloco[blocoX].tamanho() == 0){
                    this.bloco[blocoX].adicionar(blocoX);// retorno de posiçoes
                    //System.out.print(" "+blocoX+" ");
                }
            }

            //return true;
            for (int i = (tam-1); i >= 0; i--) {
                this.bloco[n].removeComeco();// removo tudo da lista
            }
        }
        //return false;
    }

    public int retorno2(int tam, int n){

        LigaBlocos blocosRetirados = new LigaBlocos();
        int indice = -1,pos = -1,tamReal = 0;
        
            for (int i = 0; i < this.qtd; i++) {//vejo onde esta o bloco em que posicao ele se encontra
                if(this.bloco[i].contem(n)){
                   pos = this.bloco[i].Indice(n);
                   indice = i;
                   tamReal = this.bloco[indice].tamanho();
                }
            }
            
            
            for (int i = (pos + 1); i < this.bloco[indice].tamanho(); i++) {//retorno os blocos acima
                int blocoX = this.bloco[indice].pegar(i);

                if(!this.bloco[blocoX].contem(blocoX)){
                    this.bloco[blocoX].adicionar(blocoX);// retorno de posiçoes
                    
                }   
            }

            if(pos > 0){//se o bloco tiver em uma posiçao maior que 0 guardar os blocos abaixo para voltar

                for (int i = 0; i < pos; i++) {
                    int blocoX = this.bloco[indice].pegar(i);
                    blocosRetirados.adicionar(blocoX);    
                }
            }

            for (int i = (tamReal-1); i >= 0; i--) {
                this.bloco[indice].removeComeco();// removo tudo da lista simplesmente encadeada
            }

            if(pos > 0 ){
                for (int i = 0; i < pos; i++) {
                    this.bloco[indice].adicionar(blocosRetirados.pegar(i));
                }
            }

        
        return indice;
    }

    public LigaBlocos retornoPilha(int tam, int n){
        LigaBlocos blocosRetirados = new LigaBlocos();

        if (tam > 0) {//vejo o tamanho do vetor

           // System.out.println("n"+n+":"+this.bloco[n].tamanho());
            int blocoX = 0;

            for (int i = 0; i < tam; i++) {
                blocoX = this.bloco[n].pegar(i);
                blocosRetirados.addComeco(blocoX);    
            }

            for (int i = 0; i < tam; i++) {
                this.bloco[n].removeComeco();
            }

            return blocosRetirados;
        }
        return null;
    }

    public LigaBlocos retornoPilha2(int n){
        LigaBlocos blocosRetirados = new LigaBlocos();
        LigaBlocos blocosRetiradosAbaixo = new LigaBlocos();

        int indice = -1,pos = -1,tamReal = 0;
        
        for (int i = 0; i < this.qtd; i++) {//vejo onde esta o bloco em que posicao ele se encontra
            if(this.bloco[i].contem(n)){
               pos = this.bloco[i].Indice(n);
               indice = i;
               tamReal = this.bloco[indice].tamanho();
            }
        }
        
        
        for (int i = (pos + 1); i < this.bloco[indice].tamanho(); i++) {//retorno os blocos acima do bloco a ser retirado
            int blocoX = this.bloco[indice].pegar(i);
            blocosRetirados.adicionar(blocoX);
        }

        if(pos > 0){//se o bloco tiver em uma posiçao maior que 0 guardar os blocos abaixo para voltar

            for (int i = 0; i < pos; i++) {
                int blocoX = this.bloco[indice].pegar(i);
                blocosRetiradosAbaixo.adicionar(blocoX);    
            }
        }

        for (int i = (tamReal-1); i >= 0; i--) {//como esta sendo utilizada a lista simplesmente encadeada esta faz a retirada do inicio
            this.bloco[indice].removeComeco();// removo tudo da lista simplesmente encadeada
        }

        if(pos > 0 ){//recoloca os blocos que foram removidos abaixo do alvo
            for (int i = 0; i < pos; i++) {
                this.bloco[indice].adicionar(blocosRetiradosAbaixo.pegar(i));
            }
        }

    
    return blocosRetirados;

    }

    public int IndiceAgora(int n){
        for (int i = 0; i < this.qtd; i++) {//vejo onde esta o bloco em que posicao ele se encontra
            if(this.bloco[i].contem(n)){
                   
                 return i;
                   
            }
        }
        return -1;
    }

    public boolean ignore(int p,int s){

        if(p == s){
            System.out.println("\na = b");
            return true;
            
        }else if(p != s){
            for (int i = 0; i < this.qtd; i++) {
                if(this.bloco[i].contem(p)&&this.bloco[i].contem(s)){//verifica se estao na mesma pilha
                    System.out.println("\na e b na mesma pilha");
                    return true;
                }
            }

        }
        
        return false;
        
    }

    public void escrita() throws IOException{

        FileWriter fw = null;
        PrintWriter saida = null;

        File arq = new File("saida.txt");//criando arquivo de saida se nao existir

        if(arq.exists()){
            System.out.println("\nO arquivo saida.txt ja existia");
        }else{
            System.out.println("\nO arquivo saida.txt nao existia");
            try {
                Path file = Paths.get("saida.txt");
                Files.createFile(file);
            } catch (IOException e) {
                System.out.println("Erro ao criar arquivo");
            }
        }
        
        String conteudo ="";//variavel que armazenara o texto do arquivo

        for (int i = 0; i < qtd; i++) {//pegando os dados para escrita no arquivo

            conteudo += i + ": " + this.bloco[i].mostrar();

            if( i < (qtd-1)){//pula linhas
                conteudo +="\n";
            }
            
        }
         

        try {
           fw = new FileWriter(arq);
           saida = new PrintWriter(fw);
           saida.println(conteudo);

        } catch (IOException e) {
            System.out.println("Erro ao escrever no arquivo");
        }finally{
            saida.close();
            fw.close();
        }

    }
}
